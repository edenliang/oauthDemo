/**
 * Created by EdenLiang on 2016/6/2.
 */
'use strict';
var Storage = {
    isLocalStorage: (window.localStorage ? true : false),
    Set: function (item, value, isJson) {
        if (this.isLocalStorage) {
            try {
                localStorage[item] = isJson ? JSON.stringify(value) : value;
            } catch (e) {
                return;
            }
        }
    },
    Get: function (item, isJson) {
        if (this.isLocalStorage) {
            try {
                return isJson ? localStorage[item] ? JSON.parse(localStorage[item]) : "" : localStorage[item];
            } catch (e) {
                return;
            }
        }
    },
    Del: function (item) {
        if (this.isLocalStorage) {
            localStorage.removeItem(item);
        }
    },
    Clear: function () {
        if (this.isLocalStorage) {
            localStorage.clear();
        }
    }
};

var QueryHelp = {
    AddQueryString: function (uri, parameters) {
        var delimiter = (uri.indexOf('?') == -1) ? '?' : '&';
        for (var parameterName in parameters) {
            var parameterValue = parameters[parameterName];
            uri += delimiter + encodeURIComponent(parameterName) + '=' + encodeURIComponent(parameterValue);
            delimiter = '&';
        }
        return uri;
    },
    ParseQueryString: function (queryString) {
        var data = {},
            pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;

        if (queryString === null) {
            return data;
        }

        pairs = queryString.split("&");

        for (var i = 0; i < pairs.length; i++) {
            pair = pairs[i];
            separatorIndex = pair.indexOf("=");

            if (separatorIndex === -1) {
                escapedKey = pair;
                escapedValue = null;
            } else {
                escapedKey = pair.substr(0, separatorIndex);
                escapedValue = pair.substr(separatorIndex + 1);
            }

            key = decodeURIComponent(escapedKey);
            value = decodeURIComponent(escapedValue);

            data[key] = value;
        }
        return data;
    },
    GetFragment: function () {
        if (window.location.hash.indexOf("#") === 0) {
            return this.ParseQueryString(window.location.hash.substr(1));
        } else {
            return {};
        }
    }

}

